package org.dp2.utilities;

import org.automation.utilities.ConfigProperty;

public class ConfigPropertyDP2 extends ConfigProperty {

    private static final String PROPERTIES_FILE = "/common.properties";

    @Override
    protected String getPropertyFilename() {
        return PROPERTIES_FILE;
    }

    public String getURL(){
        return properties.getProperty("url");
    }
}
