package org.cp.listeners;

import org.automation.utilities.CommonUtils;
import org.cp.utilities.ConfigPropertyCP;
import org.testng.ISuite;
import org.testng.ISuiteListener;

public class TestlistenersCP implements ISuiteListener {

    @Override
    public void onStart(ISuite suite) {
        CommonUtils.getInstance().createFolder(ConfigPropertyCP.getInstance().getScreenshotPath());
        CommonUtils.getInstance().createFolder(ConfigPropertyCP.getInstance().getAutomationFilePath());
    }
}
