# Veeva Assessment

This is a Test Automation Framework for Web Testing.


## Project Assessment Guidelines

1. **Multi-Module Project Structure**: This assessment project consists of multiple modules, with the test framework serving as a dependency for application testing projects.
2. **Adherence to Clean Code Principles (SOLID)**: Emphasized the importance of adhering to clean code principles, particularly the SOLID principles.
3. **Test Design Patterns**: Utilized established test design patterns, such as Page Object Model (POM) and Singleton Pattern, to maintain an organized and efficient codebase.
4. **Parallel Execution for Cross-Browser Testing**: Implemented parallel test execution to ensure that tests can run seamlessly on different web browsers.
5. **Technology Stack**:
- Programming Language: Java
- Build Automation: Maven
- Behavior-Driven Development: Cucumber
- Test Framework: TestNG
- Web Automation: Selenium WebDriver
  And other relevant technologies.
6. **Reporting Tool**: Utilized the Extent Report for comprehensive test reporting.
7. **Test Cases Overview**: This assessment project covers two distinct test cases.
- Test Case 1: CP Shop Menu Validation
    - Navigate from the CP (Core Product) home page to the "Shop" menu and select "Men’s."
    - Collect information on all available jackets across paginated pages.
    - Store each jacket's price, title, and any top seller message in a text file.
    - Attach the text file to the test report for reference.

- Test Case 2: CP New & Features Video Feed Count
    - Access the CP home page and hover over a menu item, then click on "New & Features."
    - Count the total number of video feeds present on the page.
    - Ensure that there are at least three video feeds, and report the result.
    

## Instructions to run the project : 
1. Move to the VeevaAssessment root folder.
   Run the cmd :
   ```
   mvn clean install
   ```
2. Move to the automation-framework root folder.
   Run the cmd :
   ``` 
   mvn clean install 
   ```
3. Move to the core-product-tests folder
   Run the cmd :
   ```
    mvn clean test
    ```
4. Above cmd will run the suite testng file, and its linked test cases.
5. Artifacts can be found in location : 
- _Automation Text file_ : core-product-tests/target/automationFiles/
- _Screenshot file_ : core-product-tests/target/screenshots/
- _Extent Report_ : core-product-tests/target/extent/index.html


## Report for Test Run : 
[Extent Report](./report.html)

## Automation Architecture for Framework : 
[Automation Architecture](./AutomationArchitecture.drawio)
