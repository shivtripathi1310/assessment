package org.cp.utilities;

import org.automation.utilities.ConfigProperty;
import org.automation.utilities.WebElementActions;

public class ConfigPropertyCP extends ConfigProperty {

    private static ConfigPropertyCP instance;
    private static final String PROPERTIES_FILE = "/common.properties";

    private ConfigPropertyCP(){}

    public static ConfigPropertyCP getInstance(){
        if (instance == null) {
            synchronized (WebElementActions.class) {
                if (instance == null) {
                    instance = new ConfigPropertyCP();
                }
            }
        }
        return instance;
    }

    @Override
    protected String getPropertyFilename() {
        return PROPERTIES_FILE;
    }

    public String getURL(){
        return properties.getProperty("url");
    }

    public String getScreenshotPath(){
        return properties.getProperty("screenshot.path");
    }

    public String getExtentReportPath(){
        return properties.getProperty("extent.path");
    }

    public String getAutomationFilePath(){
        return properties.getProperty("automation.file.path");
    }
}
