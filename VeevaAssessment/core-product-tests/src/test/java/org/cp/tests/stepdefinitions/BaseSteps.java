package org.cp.tests.stepdefinitions;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.cp.pages.HomePage;
import org.cp.pages.MensShopPage;
import org.cp.pages.NewsPage;
import org.cp.utilities.ConfigPropertyCP;

public class BaseSteps {

    protected static final Logger logger = LogManager.getLogger(BaseSteps.class);
    protected static final String URL = ConfigPropertyCP.getInstance().getURL();
    protected static final String filePath = ConfigPropertyCP.getInstance().getAutomationFilePath() + "/";

    protected static HomePage homePage;
    protected static MensShopPage mensShopPage;
    protected static NewsPage newsPage;
}
