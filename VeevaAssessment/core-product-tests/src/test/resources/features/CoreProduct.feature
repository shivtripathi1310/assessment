@All
Feature: Core Product Test Scenarios

  Background:
    Given I am on the CP home page
    And I accept the cookies

  @TestCase01
  Scenario: Test Case 1 - Retrieve Jacket Information from CP Home Page
    When I navigate to the 'Shop' Menu and select 'Men'
    And I search for 'Jackets'
    And I find all Jackets from all paginated pages and store each Jacket's Price, Title, and Top Seller message to a text file
    Then I attach the text file to the report

  @TestCase02
  Scenario: Test Case 2 - Count Video Feeds on CP Home Page
    When I hover over the menu item and click on 'News & Features'
    And I count the total number of Video Feeds on the page
    Then the count of video feeds should be greater than or equal to '3d'