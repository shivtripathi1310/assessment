package org.automation.reports;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.testng.ITestResult;

import java.util.*;

public final class ExtentReport {

    private ExtentReport() {
    }

    private static ExtentReports extentReports;
    private static final String REPORT_DOCUMENT_TITLE = "Reports - Veeva";
    private static final String REPORT_NAME = "Automation Report";

    private static final String REPORT_FILEPATH = "target/extent/";

    private static final String REPORT_FILENAME = "index.html";
    private static final Boolean IS_REPORT_OVERRIDE = true;

    public static void initReports() {
        if (Objects.isNull(extentReports)) {
            extentReports = new ExtentReports();
            ExtentSparkReporter spark = new ExtentSparkReporter(getReportFilepath());
            setSparkReporterConfig(spark);
            extentReports.attachReporter(spark);
            setExtentReportSystemInfo();
        }
    }

    public static void flushReport() {
        if (Objects.nonNull(extentReports)) {
            extentReports.flush();
        }
    }

    public static void createTest(ITestResult result) {
        String testcaseName = result.getTestContext().getSuite().getName() + " : " + result.getTestContext().getName();
        ExtentManager.setExtentTest(extentReports.createTest(testcaseName));
    }

    private static void setSparkReporterConfig(ExtentSparkReporter spark) {
        spark.config().setTheme(Theme.STANDARD);
        spark.config().setDocumentTitle(REPORT_DOCUMENT_TITLE);
        spark.config().setReportName(REPORT_NAME);
    }

    private static void setExtentReportSystemInfo() {
        extentReports.setSystemInfo("Automation Tester", System.getProperty("user.name"));
        extentReports.setSystemInfo("OS", System.getProperty("os.name"));
    }

    private static String getReportFilepath() {
        if (Boolean.TRUE.equals(IS_REPORT_OVERRIDE)) {
            return REPORT_FILEPATH + REPORT_FILENAME;
        } else {
            return REPORT_FILEPATH + (new Date()).toString().replace(":", "_").replace(" ", "_") + "/" + REPORT_FILENAME;
        }
    }
}
