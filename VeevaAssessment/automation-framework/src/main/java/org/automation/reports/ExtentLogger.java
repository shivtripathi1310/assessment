package org.automation.reports;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import lombok.SneakyThrows;

import java.io.File;
import java.nio.file.Files;
import java.util.Base64;

public final class ExtentLogger {

    private ExtentLogger() {
    }

    public static void trace(String message) {
        ExtentManager.getExtentTest().info(MarkupHelper.createCodeBlock(message));
    }

    public static void debug(String message) {
        ExtentManager.getExtentTest().info(MarkupHelper.createCodeBlock(message));
    }

    public static void info(String message) {
        ExtentManager.getExtentTest().info(MarkupHelper.createCodeBlock(message));
    }

    public static void info(String message, String filepath) {
        ExtentManager.getExtentTest().info(message , MediaEntityBuilder.createScreenCaptureFromPath(filepath).build());
    }

    @SneakyThrows
    public static void addScreenCaptureFromBase64String(File file, String title) {
        ExtentManager.getExtentTest().addScreenCaptureFromBase64String(Base64.getEncoder().encodeToString(Files.readAllBytes(file.toPath())), title);
    }

    public static void warn(String message) {
        ExtentManager.getExtentTest().warning(MarkupHelper.createCodeBlock(message));
    }

    public static void skip(String message) {
        ExtentManager.getExtentTest().skip(MarkupHelper.createCodeBlock(message));
    }

    public static void pass(String message) {
        ExtentManager.getExtentTest().pass(MarkupHelper.createCodeBlock(message));
    }

    public static void error(String message) {
        ExtentManager.getExtentTest().fail(MarkupHelper.createCodeBlock(message));
    }

    public static void fail(String message) {
        ExtentManager.getExtentTest().fail(MarkupHelper.createCodeBlock(message));
    }
}
