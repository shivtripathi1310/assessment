package org.automation.utilities;

import java.io.IOException;
import java.util.Properties;

public abstract class ConfigProperty {
    protected final Properties properties;

    protected ConfigProperty() {
        this.properties = new Properties();
        loadPropertiesFromFile();
    }

    protected void loadPropertiesFromFile(){
        try {
            properties.load(ConfigProperty.class.getResourceAsStream(getPropertyFilename()));
        } catch (IOException e) {
            throw new RuntimeException(String.format("Can't get properties from %s", getPropertyFilename()), e);
        }
    }

    protected abstract String getPropertyFilename();
}
