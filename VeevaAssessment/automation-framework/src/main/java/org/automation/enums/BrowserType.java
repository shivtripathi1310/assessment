package org.automation.enums;

public enum BrowserType {

    CHROME,
    FIREFOX,
    INTERNET_EXPLORER
}
