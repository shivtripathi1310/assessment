package org.cp.pages;

import lombok.SneakyThrows;
import org.automation.enums.Locator;
import org.automation.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class HomePage extends BasePage {

    @FindBy(xpath = "//img[@alt='NBA Logo']")
    WebElement nbaLogo;

    @FindBy(xpath = "//nav[@aria-label='header-secondary-menu']/ul[@role='menubar']/descendant::a/span")
    WebElement secondaryMenu;

    @FindBy(xpath = "//button[@id='onetrust-accept-btn-handler']")
    WebElement notificationAccept;

    @FindBy(xpath = "//ul[@role='menubar']")
    List<WebElement> menuBars;

    private static final String PRIMARY_MENU = "//li[@class='menu-item']/descendant::span[text()='${value}']";
    private static final String PRIMARY_MENU_ITEMS = "//li[@class='menu-item']/descendant::ul[@role='menu']/descendant::a[contains(text(),'${value}')]";
    private static final String SECONDARY_MENU_ITEMS = "//nav[@aria-label='header-secondary-menu']/ul[@role='menubar']/descendant::a[translate(text(),' ','')=\"${value}\"]";

    public HomePage open(String url) {
        actions.navigateTo(url);
        return (HomePage) openPage(HomePage.class);
    }

    public HomePage moveToPrimaryMenu(String menuName) {
        actions.visibilityOfElements(menuBars);
        String tempMenu = PRIMARY_MENU;
        tempMenu = actions.replace(tempMenu,menuName);
        actions.moveToElement(Locator.XPATH, tempMenu);
        return this;
    }

    public HomePage moveToPrimaryMenuItem(String itemName) {
        actions.visibilityOfElements(menuBars);
        String tempMenuItems = PRIMARY_MENU_ITEMS;
        tempMenuItems = actions.replace(tempMenuItems,itemName);
        actions.moveToElement(Locator.XPATH, tempMenuItems);
        return this;
    }

    public MensShopPage clickOnPrimaryMenuItem(String itemName) {
        String tempMenuItems = PRIMARY_MENU_ITEMS;
        tempMenuItems = actions.replace(tempMenuItems,itemName);
        actions.click(Locator.XPATH, tempMenuItems);

        actions.switchFocusToNewTab();

        return (MensShopPage) openPage(MensShopPage.class);
    }

    public HomePage moveToSecondaryMenu() {
        actions.moveToElement(secondaryMenu);
        return this;
    }

    public HomePage moveToSecondaryMenuItems(String itemName) {
        String tempSecondaryItems = SECONDARY_MENU_ITEMS;
        tempSecondaryItems = actions.replace(tempSecondaryItems,itemName.replace(" ",""));

        actions.moveToElement(tempSecondaryItems);
        return this;
    }

    public NewsPage clickOnSecondaryMenuItem(String itemName) {
        String tempSecondaryItems = SECONDARY_MENU_ITEMS;
        tempSecondaryItems = actions.replace(tempSecondaryItems,itemName.replace(" ",""));

        actions.click(Locator.XPATH, tempSecondaryItems);
        return (NewsPage) openPage(NewsPage.class);
    }

    public HomePage acceptTheCookies(){
        actions.click(notificationAccept);
        actions.refreshPage();
        return this;
    }

    @Override
    protected ExpectedCondition getPageLoadCondition() {
        return ExpectedConditions.titleContains("Home");
    }
}
