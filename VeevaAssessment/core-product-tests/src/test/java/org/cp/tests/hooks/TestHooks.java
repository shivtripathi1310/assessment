package org.cp.tests.hooks;

import io.cucumber.java.*;
import org.automation.reports.ExtentLogger;
import org.automation.utilities.TakeScreenshotUtil;

public class TestHooks {

    @Before
    public void before(Scenario scenario){
        ExtentLogger.info("STARTED : " + scenario.getName());
    }

    @After
    public void after(Scenario scenario){
        ExtentLogger.info("FINISHED : " + scenario.getName());
    }

    @BeforeStep
    public void beforeSteps(){
//        ExtentLogger.info(step.toString());
    }

    @AfterStep
    public void afterSteps(){
//        ExtentLogger.info(step.toString());
        TakeScreenshotUtil.getInstance().takeScreenShot();
    }
}
