package org.automation.driver;

import org.automation.enums.BrowserType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class DriverFactory {

    public static WebDriver getDriver(BrowserType browserType) {

        switch (browserType) {
            case CHROME:
                DriverManager.setWebDriver(new ChromeDriver());
                return DriverManager.getDriver();
            case FIREFOX:
                DriverManager.setWebDriver(new FirefoxDriver());
                return DriverManager.getDriver();
            case INTERNET_EXPLORER:
                DriverManager.setWebDriver(new InternetExplorerDriver());
                return DriverManager.getDriver();
            default:
                throw new IllegalArgumentException("Unsupported driver: " + browserType.name());
        }

    }
}
