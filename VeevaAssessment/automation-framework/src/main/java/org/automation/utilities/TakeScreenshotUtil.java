package org.automation.utilities;


import lombok.SneakyThrows;
import org.automation.driver.DriverManager;
import org.automation.reports.ExtentLogger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TakeScreenshotUtil {

    private static TakeScreenshotUtil instance;
    private static final String FILE_PATH = "target/screenshot/";

    private TakeScreenshotUtil() {
    }

    public static TakeScreenshotUtil getInstance() {
        if (instance == null) {
            synchronized (TakeScreenshotUtil.class) {
                if (instance == null) {
                    instance = new TakeScreenshotUtil();
                }
            }
        }
        return instance;
    }

    @SneakyThrows
    public void takeScreenShot() {
        CommonUtils.getInstance();
        String name = FILE_PATH + "IMAGE_" + CommonUtils.getInstance().generateRandomAlphanumeric(5) + "_" + System.currentTimeMillis();

        File source = ((TakesScreenshot) DriverManager.getDriver()).getScreenshotAs(OutputType.FILE);
        Files.copy(source.toPath(), Paths.get(name));
        ExtentLogger.addScreenCaptureFromBase64String(source, name);
    }
}
