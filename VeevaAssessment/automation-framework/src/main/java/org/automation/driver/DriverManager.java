package org.automation.driver;

import org.openqa.selenium.WebDriver;

public class DriverManager {

    public static ThreadLocal<WebDriver> driver = new ThreadLocal<>();

    public static WebDriver getDriver() {
        return driver.get();
    }

    public static void setWebDriver(WebDriver driver) {
        DriverManager.driver.set(driver);
    }

    public static void clean(){
        driver.remove();
    }
}
