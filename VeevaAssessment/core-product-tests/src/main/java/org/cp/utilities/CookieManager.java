package org.cp.utilities;

public class CookieManager {

    private static ThreadLocal<Boolean> isCookieAccepted = new ThreadLocal<>();

    public static void setCookieAccepted(boolean value){
        isCookieAccepted.set(value);
    }

    public static boolean getCookieAccepted(){
        return isCookieAccepted.get();
    }
}
