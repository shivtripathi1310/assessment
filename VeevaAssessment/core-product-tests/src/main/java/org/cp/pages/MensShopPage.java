package org.cp.pages;

import lombok.SneakyThrows;
import org.automation.enums.Locator;
import org.automation.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class MensShopPage extends BasePage {

    @FindBy(xpath = "//div[@class='main-bar show-for-large']/descendant::img[@alt='Golden State Warriors Official Online Shop']")
    WebElement menShopLogo;

    @FindBy(xpath = "//input[@id='typeahead-input-desktop']")
    WebElement searchBar;

    @FindBy(xpath = "//button[@aria-label='Search Product']")
    WebElement searchButton;

    @FindBy(xpath = "//div[@class='modal-mobile-background no-background']/descendant::img[@loading='lazy' and @alt='25% Off Sitewide']")
    WebElement advLogo;

    private static final String ITEM_COUNT = "//div[@data-talos='itemCount']";
    private static final String PRODUCT_TITLES = "//div[@class='product-card-title']/a";
    private static final String NEXT_PAGE = "(//i[@aria-label='next page' and @aria-disabled='false'])[1]";
    private static final String PRICE_TAG = "//div[@class='product-card-title']/a[translate(text(),' ','')=\"${value}\"]/../preceding-sibling::div/div[@class='price-card']/descendant::span[@class='money-value']//span[@class='sr-only']";
    private static final String TOP_SELLER_MESSAGE_HEADING = "//div[@class='product-card-title']/a[translate(text(),' ','')=\"${value}\"]/../following-sibling::div[@class='product-vibrancy-container']/descendant::span/span[@class='top-seller-vibrancy-message']";
    private static final String TOP_SELLER_MESSAGE_DETAIL = "//div[@class='product-card-title']/a[translate(text(),' ','')=\"${value}\"]/../following-sibling::div[@class='product-vibrancy-container']/descendant::span/span[@class='top-seller-vibrancy-message hide-for-small-only']";

    public String getPriceTag(String productName) {
        String tempPriceTag = PRICE_TAG;
        tempPriceTag = actions.replace(tempPriceTag, productName.replace(" ", ""));
        return actions.getTextFromJS(tempPriceTag);
    }

    public String getTopSellerMessage(String productName) {
        String tempTopSellerMessageHeading = TOP_SELLER_MESSAGE_HEADING;
        String tempTopSellerMessageDetail = TOP_SELLER_MESSAGE_DETAIL;

        tempTopSellerMessageHeading = actions.replace(tempTopSellerMessageHeading, productName.replace(" ", ""));
        tempTopSellerMessageDetail = actions.replace(tempTopSellerMessageDetail, productName.replace(" ", ""));

        if (actions.isElementPresent(Locator.XPATH, tempTopSellerMessageHeading)) {
            return actions.getTextFromJS(tempTopSellerMessageHeading) + " " + actions.getText(tempTopSellerMessageDetail);
        }

        return null;
    }

    public MensShopPage enterValueInSearchBar(String text) {
        actions.click(searchBar);
        actions.sendKeys(searchBar, text);
        return this;
    }

    public MensShopPage clickSearchButton() {
        actions.click(searchButton);
        return this;
    }

    public List<String> getListOfProductDetails() {
        List<WebElement> elements = actions.findElements(Locator.XPATH, PRODUCT_TITLES);
        return elements.stream()
                .map(actions::getTextFromJS)
                .collect(Collectors.toList());
    }

    public List<LinkedHashMap<String, String>> getProductPriceAndTopSellerMessageDetails() {
        List<LinkedHashMap<String, String>> list = new LinkedList<>();

        do {
            List<String> titles = getListOfProductDetails();
            for (String title : titles) {
                LinkedHashMap<String, String> map = new LinkedHashMap<>();

                String price = getPriceTag(title);
                String message = getTopSellerMessage(title);

                map.put("title", title);
                map.put("price", price);
                map.put("message", message);

                list.add(map);
            }
            if(actions.isElementPresent(Locator.XPATH, NEXT_PAGE)) {
                actions.click(NEXT_PAGE);
            }
            else
                break;
        } while (true);

        return list;
    }

    public Integer getItemCount(){
        String countStr = actions.getText(ITEM_COUNT);
        return Integer.parseInt(countStr.substring(countStr.indexOf("of") + 3));
    }

    @SneakyThrows
    public MensShopPage waitForInvisibilityOfAdvLogo(){
        Thread.sleep(3000);
        actions.waitForInvisibility(advLogo);
        return this;
    }

    @Override
    protected ExpectedCondition getPageLoadCondition() {
        return ExpectedConditions.titleContains("Men");
    }
}
