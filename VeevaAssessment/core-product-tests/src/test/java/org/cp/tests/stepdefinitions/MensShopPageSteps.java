package org.cp.tests.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.SneakyThrows;
import org.automation.reports.ExtentLogger;
import org.automation.utilities.CommonUtils;
import org.testng.Assert;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

public class MensShopPageSteps extends BaseSteps {

    private List<LinkedHashMap<String, String>> listOfProductDetails = new LinkedList<>();

    @When("I search for {string}")
    public void search_for_product(String product) {
        mensShopPage.waitForInvisibilityOfAdvLogo()
                .enterValueInSearchBar(product)
                .clickSearchButton();
    }

    @When("I find all Jackets from all paginated pages and store each Jacket's Price, Title, and Top Seller message to a text file")
    public void i_find_all_jackets_from_all_paginated_pages_store_each_jacket_s_price_title_and_top_seller_message() {
        listOfProductDetails = mensShopPage.getProductPriceAndTopSellerMessageDetails();
        int count = mensShopPage.getItemCount();
        ExtentLogger.info("Item Count : " + count);
        Assert.assertEquals(listOfProductDetails.size(), count);
    }

    @SneakyThrows
    @Then("I attach the text file to the report")
    public void i_attach_the_text_file_to_the_report() {
        String filename = "report_" + CommonUtils.getInstance().generateRandomAlphanumeric(5) + "_" + System.currentTimeMillis() + ".txt";
        String file = filePath + filename;
        StringBuilder builder = new StringBuilder();

        builder.append("Title , Price ,  Message\n");
        for (LinkedHashMap<String, String> map : listOfProductDetails) {
            builder.append(map.get("title")).append(" , ")
                    .append(map.get("price")).append(" , ")
                    .append(map.get("message")).append("\n");
        }

        Files.write(Paths.get(file), builder.toString().getBytes());
        ExtentLogger.info("Text File Attached :", file);
        ExtentLogger.info(builder.toString());
    }
}
