package org.automation.utilities;

import lombok.SneakyThrows;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.automation.driver.DriverManager;
import org.automation.enums.Locator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.Set;


public class WebElementActions {

    private static final Logger logger = LogManager.getLogger(WebElementActions.class);
    private static WebElementActions actions;
    private WebDriverWait wait;

    private WebElementActions() {
        wait = new WebDriverWait(DriverManager.getDriver(), Duration.ofSeconds(20));
    }

    public static WebElementActions getInstance() {
        if (actions == null) {
            synchronized (WebElementActions.class) {
                if (actions == null) {
                    actions = new WebElementActions();
                }
            }
        }
        return actions;
    }

    public WebElement findElement(By by) {
        WebElement element = DriverManager.getDriver().findElement(by);
        highlightElement(element);
        return element;
    }

    public WebElement findElement(Locator locator, String path) {
        By by = getBy(locator, path);
        return findElement(by);
    }

    public List<WebElement> findElements(By by) {
        return DriverManager.getDriver().findElements(by);
    }

    public List<WebElement> findElements(Locator locator, String path) {
        By by = getBy(locator, path);
        return findElements(by);
    }

    public void click(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
        scrollToElement(element);
        wait.until(ExpectedConditions.elementToBeClickable(element));
        highlightElement(element);
        element.click();
    }

    public void click(Locator locator, String target) {
        WebElement element = findElement(locator, target);
        click(element);
    }

    public void click(String target) {
        WebElement element = findElement(Locator.XPATH, target);
        click(element);
    }

    public void sendKeys(WebElement element, String text) {
        wait.until(ExpectedConditions.visibilityOf(element));
        highlightElement(element);
        element.sendKeys(text);
    }

    public void navigateTo(String url) {
        DriverManager.getDriver().navigate().to(url);
    }

    public void moveToElement(WebElement target) {
        scrollToElement(target);
        wait.until(ExpectedConditions.visibilityOf(target));
        highlightElement(target);

        Actions actions = new Actions(DriverManager.getDriver());
        actions.moveToElement(target).build().perform();
    }

    public void moveToElement(String target) {
        WebElement element = waitForStaleElement(Locator.XPATH, target, 20);
        moveToElement(element);
    }

    public void moveToElement(Locator locator, String target) {
        WebElement element = waitForStaleElement(locator, target, 20);
        moveToElement(element);
    }

    @SneakyThrows
    private WebElement waitForStaleElement(Locator locator, String target, long timeInSec) {
        long endTimeMillis = System.currentTimeMillis() + (timeInSec * 1000);
        WebElement element;
        do {
            Thread.sleep(10);
            logger.info("Trying to get element... : " + target);
            try {
                element = DriverManager.getDriver().findElement(getBy(locator, target));
                return element;
            } catch (Exception exception) {
                element = null;
            }
        }
        while (element == null && System.currentTimeMillis() < endTimeMillis);

        return null;
    }

    public String replace(String xpath, String value) {
        return xpath.replace("${value}", value);
    }

    public String getText(String xpath) {
        WebElement element = waitForStaleElement(Locator.XPATH, xpath, 10);
        scrollToElement(element);
        highlightElement(element);
        return element.getText();
    }

    public void performJavascriptAction(String script, WebElement element) {
        JavascriptExecutor jsExecutor = (JavascriptExecutor) DriverManager.getDriver();
        jsExecutor.executeScript(script, element);
    }

    public String getTextFromJS(WebElement element) {
        String script = "return arguments[0].innerText;";
        JavascriptExecutor jsExecutor = (JavascriptExecutor) DriverManager.getDriver();

        return (String) jsExecutor.executeScript(script, element);
    }

    public void scrollToElement(WebElement webElement) {
        Actions actions = new Actions(DriverManager.getDriver());
        actions.scrollToElement(webElement);
    }

    public void scrollToElementInJS(String path) {
        String script = "arguments[0].scrollIntoView();";
        performJavascriptAction(script, findElement(Locator.XPATH, path));
    }

    public void sendKeysInJS(WebElement webElement, String text) {
        wait.until(ExpectedConditions.visibilityOf(webElement));
        String script = "arguments[0].value='" + text + "';";
        performJavascriptAction(script, webElement);
    }

    public String getTextFromJS(String xpath) {
        WebElement element = findElement(Locator.XPATH, xpath);
        return getTextFromJS(element);
    }

    public String getText(WebElement element) {
        scrollToElement(element);
        return element.getText();
    }

    public By getBy(Locator locator, String path) {
        By by;
        switch (locator) {
            case XPATH:
                by = By.xpath(path);
                break;
            case CSS:
                by = By.cssSelector(path);
                break;
            case ID:
                by = By.id(path);
                break;
            case TAGNAME:
                by = By.tagName(path);
                break;
            default:
                by = By.xpath(path);
        }
        return by;
    }

    public void refreshPage() {
        DriverManager.getDriver().navigate().refresh();
    }

    public String getLink(String elementXpath) {
        return findElement(getBy(Locator.XPATH, elementXpath)).getAttribute("href");
    }

    public boolean isElementPresent(Locator locator, String path) {
        try {
            findElement(locator, path);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public void highlightElement(WebElement webElement) {
        String borderHighlight = "arguments[0].style.border='3px solid red'";
        performJavascriptAction(borderHighlight, webElement);
    }



    public void switchFocusToNewTab() {
        Set<String> windowHandles = DriverManager.getDriver().getWindowHandles();
        String originalWindowHandle = DriverManager.getDriver().getWindowHandle();

        for (String handle : windowHandles) {
            if (originalWindowHandle.equals(handle))
                DriverManager.getDriver().close();
            else
                DriverManager.getDriver().switchTo().window(handle);
        }
    }

    public void visibilityOfElement(Locator locator, String path) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(getBy(locator, path)));
    }

    public void visibilityOfElements(List<WebElement> elements) {
        wait.until(ExpectedConditions.visibilityOfAllElements(elements));
    }

    public void waitForInvisibility(WebElement element) {
        wait.until(ExpectedConditions.invisibilityOf(element));
    }

    public void waitForInvisibility(Locator locator, String path) {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(getBy(locator,path)));
    }
}
