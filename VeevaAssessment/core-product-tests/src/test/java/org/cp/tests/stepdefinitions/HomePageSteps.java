package org.cp.tests.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.cp.pages.HomePage;
import org.cp.utilities.CookieManager;

import java.util.Objects;

public class HomePageSteps extends BaseSteps {


    @Given("I am on the CP home page")
    public void i_am_on_the_cp_home_page() {
        homePage = new HomePage().open(URL);
    }

    @Given("I accept the cookies")
    public void i_accept_the_cookies() {
        if(!CookieManager.getCookieAccepted()){
            homePage.acceptTheCookies();
            CookieManager.setCookieAccepted(true);
        }
    }

    @When("I navigate to the {string} Menu and select {string}")
    public void i_navigate_to_the_menu_and_select(String primaryMenu, String primaryMenuItem) {
        mensShopPage = homePage.moveToPrimaryMenu(primaryMenu)
                .moveToPrimaryMenuItem(primaryMenuItem)
                .moveToPrimaryMenu(primaryMenu)
                .clickOnPrimaryMenuItem(primaryMenuItem);
    }

    @When("I hover over the menu item and click on {string}")
    public void i_hover_over_the_menu_item_and_click_on_feed(String secondaryMenuItem) {
        newsPage = homePage.moveToSecondaryMenu()
                .moveToSecondaryMenuItems(secondaryMenuItem)
                .moveToSecondaryMenu()
                .clickOnSecondaryMenuItem(secondaryMenuItem);
    }
}
