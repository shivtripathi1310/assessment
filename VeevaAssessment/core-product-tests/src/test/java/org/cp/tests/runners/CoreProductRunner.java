package org.cp.tests.runners;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.automation.driver.DriverFactory;
import org.automation.driver.DriverManager;
import org.automation.enums.BrowserType;
import org.cp.utilities.CookieManager;
import org.testng.annotations.*;

@CucumberOptions(features = "src/test/resources/features/CoreProduct.feature",
        glue = {"org.cp.tests.stepdefinitions","org.cp.tests.hooks"},
        plugin = {"pretty"},
        monochrome = true,
        publish = true
)
public class CoreProductRunner extends AbstractTestNGCucumberTests {

    @Parameters("browser")
    @BeforeTest
    public void setup(String browser) {
        DriverFactory.getDriver(BrowserType.valueOf(browser.toUpperCase()));
        CookieManager.setCookieAccepted(false);
        DriverManager.getDriver().manage().deleteAllCookies();
        DriverManager.getDriver().manage().window().maximize();
    }

    @AfterTest
    public void teardown() {
        DriverManager.getDriver().quit();
        DriverManager.clean();
    }
}
