package org.automation.enums;

public enum Locator {
    XPATH,
    ID,
    CSS,
    TAGNAME
}
