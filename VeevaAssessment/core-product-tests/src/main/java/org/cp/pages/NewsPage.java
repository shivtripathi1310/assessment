package org.cp.pages;

import org.automation.pages.BasePage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class NewsPage extends BasePage {

    @FindBy(xpath = "//h3[text()='NEWS']")
    WebElement newsLogo;

    @FindBy(xpath = "//div[contains(@class,'ColumnsComponents_container')]//h3[text()='VIDEOS']/ancestor::div[contains(@class,'ColumnsComponents_container')]/descendant::li//time/span")
    List<WebElement> videoListTime;


    public int getCountOfVideoFeeds(){
        return videoListTime.size();
    }

    public Integer getCountOfVideoFeedWithTimeGreaterThanEqualTo(String time){
        return (int) videoListTime.stream()
                .map(actions::getText)
                .filter(text -> {
                    if(text.equals(time))
                        return true;
                    else if(text.charAt(text.length()-1) == time.charAt(time.length()-1)){
                        return Integer.parseInt(text.substring(0, text.length() - 1)) > Integer.parseInt(time.substring(0, time.length() - 1));
                    }else if (text.charAt(text.length()-1)=='h' && time.charAt(time.length()-1)=='d')
                        return false;
                    else
                        return true;
                }).count();
    }

    @Override
    protected ExpectedCondition getPageLoadCondition() {
        return ExpectedConditions.visibilityOf(newsLogo);
    }
}
