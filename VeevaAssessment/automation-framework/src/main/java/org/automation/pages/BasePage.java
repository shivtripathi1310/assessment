package org.automation.pages;

import org.automation.driver.DriverManager;
import org.automation.utilities.WebElementActions;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public abstract class BasePage<T> {

    protected WebDriver driver;
    protected static final WebElementActions actions = WebElementActions.getInstance();
    private static final long LOAD_TIMEOUT = 20;
    private static final int AJAX_ELEMENT_TIMEOUT = 10;

    protected BasePage() {
        this.driver = DriverManager.getDriver();
    }

    protected T openPage(Class<T> className){
        T page = null;
        try {
            AjaxElementLocatorFactory ajaxElemFactory = new AjaxElementLocatorFactory(driver, AJAX_ELEMENT_TIMEOUT);
            page = PageFactory.initElements(driver, className);
            PageFactory.initElements(ajaxElemFactory, page);

            ExpectedCondition pageLoadCondition = ((BasePage) page).getPageLoadCondition();
            waitForPageToLoad(pageLoadCondition);

        } catch (NoSuchElementException e) {
            throw new IllegalStateException(String.format("This is not the %s page", className.getSimpleName()));
        }
        return page;
    }

    protected abstract ExpectedCondition getPageLoadCondition();

    private void waitForPageToLoad(ExpectedCondition pageLoadCondition) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(LOAD_TIMEOUT));
        wait.until(pageLoadCondition);
    }
}
