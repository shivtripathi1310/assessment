package org.cp.tests.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.automation.reports.ExtentLogger;

public class NewsPageSteps extends BaseSteps {

    @When("I count the total number of Video Feeds on the page")
    public void i_count_the_total_number_of_video_feeds_on_the_page() {
        ExtentLogger.info("Total Video Feeds : " + newsPage.getCountOfVideoFeeds());
    }

    @Then("the count of video feeds should be greater than or equal to {string}")
    public void the_count_of_video_feeds_should_be_greater_than_or_equal_to(String time) {
        ExtentLogger.info("Total Video Feeds In Time >= "+ time +" is : " + newsPage.getCountOfVideoFeedWithTimeGreaterThanEqualTo(time));
    }
}
